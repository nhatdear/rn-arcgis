import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity,DeviceEventEmitter } from 'react-native';
import ArcGISMap from './ArcGIS/ArcGISMap';
import firebase from 'react-native-firebase'
import mapSources from './MockData/mapsources.json'
import mapDrawing from './MockData/mapdrawings.json'
import Subscribable from 'Subscribable';
import {Actions} from 'react-native-router-flux'
export default class App extends React.Component {
	mixins = [Subscribable]
	state={
		data : null,
		mapSource : null,
		hasPrev: false,
		hasNext: false,
		isStart: false,
		currentIndex: -1,
		currentMap : '',
		GetDataOfflineTime:0,
		LoadMapTime:0
	}

	lastAttemp = 0
	constructor(props){
		super(props)
		firebase.database().goOffline();
		firebase.database().ref("mapSources").set(mapSources[0]).then().catch(e=>console.log('Error on saving MapSource Data'))
		firebase.database().ref("mapSources").once('value',snap => {
			this.setState({mapSource:snap.val()})
		}).then()
		firebase.database().ref("mapDrawing").set(mapDrawing).then().catch(e=>console.log('Error on saving MapDrawing Data'))
		this.onGetFBData = this.onGetFBData.bind(this)
		let _this = this
		DeviceEventEmitter.addListener('onChange', function(e) {
			console.log(e)
			if (e.message === "Map load start") {
				_this.setState({LoadMapTime:new Date()})
			} else if (e.message === "Map load done") {
				_this.setState({LoadMapTime:new Date() - _this.state.LoadMapTime});
			} else if (e.message === "IN_PROGRESS") {
				if (_this.state.LoadMapTime !== 0) {
					_this.setState({DrawMapTime:new Date()})
				}
			} else if (e.message === "COMPLETED") {
				if (_this.state.LoadMapTime !== 0) {
					_this.setState({DrawMapTime:new Date() - _this.state.DrawMapTime})
					let str = "Offline Data: " + Math.floor(_this.state.GetDataOfflineTime) + " miliseconds\n"
								+ "Load map: " + Math.floor(_this.state.LoadMapTime) + " miliseconds\n"
								+ "Draw map: " + Math.floor(_this.state.DrawMapTime) +   " miliseconds\n";
								alert(str)
					_this.setState({GetDataOfflineTime:0,LoadMapTime:0,DrawMapTime:0})
					Actions.pop()
				}
			}
			// if (e.message === "COMPLETED") {
			// 	if (Actions.currentScene === "progress") Actions.pop()
			// }
		});
	}

	onGetFBData(index){
		console.log(index)
		if (index === this.state.currentIndex) return;
		Actions.progress()
		var t0 = new Date()
		let _this = this;
		firebase.database().ref("mapDrawing").once('value',snap => {
			var array = snap.val();
			if (index < array.length - 1 && index > 0) {
				hasNext = true;
				hasPrev = true;
			} else if (index == array.length - 1) {
				hasNext = false;
				hasPrev = true;
			} else if (index == 0) {
				hasNext = true;
				hasPrev = false;
			}
			var data = snap.val()[index];
			var t1 = new Date()
			var GetDataOffline = t1 - t0
			_this.setState({GetDataOfflineTime:GetDataOffline});
			_this.setState({data:data,currentIndex:index, hasNext: hasNext, hasPrev: hasPrev, isStart : true, currentMap: data.trekName})
		}).then()
	}

	onNext(){
		this.onGetFBData(this.state.currentIndex + 1);
	}

	onPrev(){
		console.log('onPrevious')
		this.onGetFBData(this.state.currentIndex - 1);
	}

	onExtentChange(v){
		debugger
		alert(v)
	}
	render() {  
		return (
			<View style={styles.container}>
				<Text style={styles.title}>Welcome to ArcGis React Native</Text>
				<View style={{height:400,width:'100%'}}>
					<ArcGISMap ref="map" onExtentChange={this.onExtentChange.bind(this)} mapData={this.state.data} mapSource={this.state.mapSource} style={styles.map}/>
					<Text style={styles.mapName}>{"Trek name: " +this.state.currentMap}</Text>
				</View>
				<View style={{flexDirection:'row'}}>
					<TouchableOpacity disabled={!this.state.hasPrev} onPress={this.onPrev.bind(this)} style={this.state.hasPrev? styles.buttonActive : styles.buttonInActive}>
						<Text style={{color:'white'}}>Previous</Text>
					</TouchableOpacity>
					<TouchableOpacity disabled={!this.state.hasNext} onPress={this.onNext.bind(this)} style={this.state.hasNext? styles.buttonActive : styles.buttonInActive}>
						<Text style={{color:'white'}}>Next</Text>
					</TouchableOpacity>
				</View>
				<View style={{width:200,height:70}}>
					<TouchableOpacity onPress={() => this.onGetFBData(0)} style={styles.buttonActive}>
							<Text style={{color:'white'}}>Start</Text>
					</TouchableOpacity>
				</View>
			</View>
		);
  	}
}

const buttonBase = {
	flex:1,
		marginTop:20,
		height:40,
		margin:10,
		borderWidth:1,
		borderRadius:4,
		alignItems:'center',
		justifyContent:'center'
}
const styles = StyleSheet.create({
	container: {
		flex: 1,
		height:'100%',
		width:'100%',
		alignItems: 'center'
	},
	map: {
        flex: 1
	},
	mapName: {
		fontSize:20,
		textAlign:'center',
		justifyContent:'center',
		alignItems:'center',
		padding:5,
		fontWeight:"700"
	},
	title: {
		fontSize:22,
		padding:15,
		fontWeight:"700"
	},
	buttonActive: {
		...buttonBase,
		backgroundColor: 'green'
	},
	buttonInActive:{
		...buttonBase,
		backgroundColor: 'grey'
	}
});
