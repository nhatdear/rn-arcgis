import React from 'react'
import {requireNativeComponent, View, NativeModules} from 'react-native';

import PropTypes from 'prop-types'
import Subscribable from 'Subscribable';
var customView =  {
  name: 'ArcGISMap',
  propTypes: {
    ...View.propTypes,
    mapSource: PropTypes.object,
    mapData: PropTypes.object,
    layers: PropTypes.array
  },
  render() {
    return (<ArcGISMap {...this.props} />);
  }
}
var ArcGISMap = requireNativeComponent('ArcGISMap',customView);


export default ArcGISMap

