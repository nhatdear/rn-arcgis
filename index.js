import React from 'react'
import {AppRegistry, View, Text, Image} from 'react-native';
import App from './App/App';
import {
    Actions,
    Scene,
    Overlay,
    Router,
    Reducer,
    Stack,
    Drawer,
    Tabs,
    ActionConst,
    Lightbox
} from 'react-native-router-flux'

const reducerCreate = params => {
    const defaultReducer = new Reducer(params);
    return (state, action) => {
        return defaultReducer(state, action);
    };
};

class NavigationRouter extends React.Component {
    render() {
        return (
            <Router
                createReducer={reducerCreate}
                >
                <Lightbox>
                    <Stack key="root">
                        <Stack initial hideNavBar key='home'>
                            <Scene component={App} />
                        </Stack>
                    </Stack>
                    <Scene key="progress" component={ProgressBox} />
                </Lightbox>
            </Router>
        )
    }
}

class ProgressBox extends React.Component {
    render() {
        return (
            <View style={[{alignItems:'center',justifyContent:'center', height: '100%', width: '100%', position: 'absolute' }]}>
                <View style={{ height: '100%', width: '100%', position: 'absolute', opacity: 0.5, backgroundColor: 'grey' }}></View>
                <Image source={require('./App/Images/loading_small.gif')} style={{ width: 200, height: 200 }} />
            </View>
        )
    }
}

AppRegistry.registerComponent('rnarcgis', () => NavigationRouter );
