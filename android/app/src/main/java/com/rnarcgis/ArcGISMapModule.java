package com.rnarcgis;

import android.util.Log;

import com.esri.arcgisruntime.mapping.ArcGISMap;
import com.esri.arcgisruntime.mapping.view.MapView;
//import com.esri.core.geometry.GeometryEngine;
//import com.esri.core.geometry.Point;
//import com.esri.core.geometry.SpatialReference;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

/**
 * Created by nhatd on 12/6/2017.
 */

public class ArcGISMapModule extends ReactContextBaseJavaModule{
    public static final String REACT_CLASS = "ArcGISMapModule";

    private MapView mapView;

    public ArcGISMapModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    public void setMapView(MapView mapView) {
        this.mapView = mapView;
    }

    @ReactMethod
    public void setLevel(float level) {
        Log.v(REACT_CLASS, "set level: " + level);
    }

    @ReactMethod
    public void setCenterWGS84(float x, float y) {
        Log.v(REACT_CLASS, "set center: " + x + ", " + y);
    }
}
