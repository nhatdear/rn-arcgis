package com.rnarcgis;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.esri.arcgisruntime.arcgisservices.LevelOfDetail;
import com.esri.arcgisruntime.arcgisservices.TileInfo;
import com.esri.arcgisruntime.concurrent.ListenableFuture;
import com.esri.arcgisruntime.data.TileKey;
import com.esri.arcgisruntime.geometry.Envelope;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.geometry.SpatialReference;
import com.esri.arcgisruntime.geometry.SpatialReferences;
import com.esri.arcgisruntime.layers.ArcGISMapImageLayer;
import com.esri.arcgisruntime.layers.ArcGISTiledLayer;
import com.esri.arcgisruntime.layers.ImageTiledLayer;
import com.esri.arcgisruntime.layers.ServiceImageTiledLayer;
import com.esri.arcgisruntime.layers.WmtsLayer;
import com.esri.arcgisruntime.loadable.LoadStatusChangedEvent;
import com.esri.arcgisruntime.loadable.LoadStatusChangedListener;
import com.esri.arcgisruntime.mapping.ArcGISMap;
import com.esri.arcgisruntime.mapping.Basemap;
import com.esri.arcgisruntime.mapping.Viewpoint;
import com.esri.arcgisruntime.mapping.view.LocationDisplay;
import com.esri.arcgisruntime.mapping.view.MapView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import bolts.CancellationToken;
import bolts.Task;
import okhttp3.OkHttpClient;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        String uri = "@android.resource://" + this.getPackageName() + "/drawable/cbmt";
//        WmtsLayer layer = new WmtsLayer(uri,"CBMT", TileInfo.ImageFormat.PNG);
//        layer.setOpacity(1f);
//        layer.loadAsync();
//        layer.addLoadStatusChangedListener(new LoadStatusChangedListener() {
//            @Override
//            public void loadStatusChanged(LoadStatusChangedEvent loadStatusChangedEvent) {
//                Log.v("aaa",loadStatusChangedEvent.getNewLoadStatus().name());
//            }
//        });
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
//        final ArcGISMapImageLayer arcGISMapImageLayer = new ArcGISMapImageLayer(uri);
//        arcGISMapImageLayer.setImageFormat(ArcGISMapImageLayer.ImageFormat.PNG);
//        arcGISMapImageLayer.setOpacity(1f);
//        arcGISMapImageLayer.loadAsync();
//        arcGISMapImageLayer.addDoneLoadingListener(new Runnable() {
//            @Override
//            public void run() {
//                Log.d("",arcGISMapImageLayer.getImageFormat().name());
//            }
//        });
//        arcGISMapImageLayer.addLoadStatusChangedListener(new LoadStatusChangedListener() {
//            @Override
//            public void loadStatusChanged(LoadStatusChangedEvent loadStatusChangedEvent) {
//                Log.d("",loadStatusChangedEvent.getNewLoadStatus().toString());
//            }
//        });
        // inflate MapView from layout
//        final MapView mMapView = (MapView) findViewById(R.id.mapView);
//        ArcGISMap mMap = new ArcGISMap(Basemap.Type.TOPOGRAPHIC,56.130366,-106.346771,7);
//        final OpenMap openMap = new OpenMap();
//        String url = "http://geogratis.gc.ca/maps/CBMT?service=wms&version=1.1.1&request=GetMap&format=image/png&bgcolor=0xFFFFFF&exceptions=application/vnd.ogc.se_inimage&srs=EPSG:900913&layers=CBMT&bbox=-11897270.578531113,7514065.628545966,-11584184.510675032,7827151.696402047&width=256&height=256";
//        RequestQueue mRequestQueue;
//        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap
//        Network network = new BasicNetwork(new HurlStack());
//        mRequestQueue = new RequestQueue(cache, network);
//        mRequestQueue.start();
//
//        ImageRequest stringRequest = new ImageRequest(url.toString(),
//                new Response.Listener<Bitmap>() {
//                    @Override
//                    public void onResponse(Bitmap response) {
//                        response.getByteCount();
//                        File SDCardRoot = Environment.getExternalStorageDirectory().getAbsoluteFile();
//                        String filename = "downloadedFile.png";
//                        Log.i("Local filename:", "" + filename);
//                        File file = new File(SDCardRoot, filename);
//                        try {
//                            if (file.createNewFile()) {
//                                file.createNewFile();
//                            }
//
//                            FileOutputStream fileOutput = new FileOutputStream(file);
//                            response.compress(Bitmap.CompressFormat.PNG, 100, fileOutput);
//                            openMap.url = file.getAbsolutePath();
//                            openMap.loadAsync();
//                            openMap.addDoneLoadingListener(new Runnable() {
//                                @Override
//                                public void run() {
//                                    Log.d("aaa", openMap.getId());
//                                }
//                            });
//                            final ArcGISMapImageLayer arcGISMapImageLayer = new ArcGISMapImageLayer(file.getAbsolutePath());
//                            arcGISMapImageLayer.setImageFormat(ArcGISMapImageLayer.ImageFormat.PNG);
//                            arcGISMapImageLayer.setOpacity(1f);
//                            arcGISMapImageLayer.loadAsync();
//                            arcGISMapImageLayer.addDoneLoadingListener(new Runnable() {
//                                @Override
//                                public void run() {
//                                    Log.d("",arcGISMapImageLayer.getImageFormat().name());
//                                }
//                            });
//                            arcGISMapImageLayer.addLoadStatusChangedListener(new LoadStatusChangedListener() {
//                                @Override
//                                public void loadStatusChanged(LoadStatusChangedEvent loadStatusChangedEvent) {
//                                    Log.d("",loadStatusChangedEvent.getNewLoadStatus().toString());
//                                }
//                            });
//                            ArcGISMap mMap = new ArcGISMap();
//                            mMap.setBasemap(new Basemap(arcGISMapImageLayer));
//                            mMapView.setMap(mMap);
//
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, 0, 0, null, null);
//        mRequestQueue.add(stringRequest);

    }
}
