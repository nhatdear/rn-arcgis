package com.rnarcgis;

import android.net.Uri;

import com.esri.arcgisruntime.arcgisservices.LevelOfDetail;
import com.esri.arcgisruntime.arcgisservices.TileInfo;
import com.esri.arcgisruntime.data.TileKey;
import com.esri.arcgisruntime.geometry.Envelope;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.geometry.SpatialReferences;
import com.esri.arcgisruntime.layers.ServiceImageTiledLayer;

import java.util.ArrayList;

import bolts.CancellationToken;
import bolts.Task;

/**
 * Created by cimbvn on 12/9/17.
 */

public class OpenMap extends ServiceImageTiledLayer {
    public String url;
    public OpenMap() {
        super(CreateTileInfo(), new Envelope(-11897270.578531113, 7514065.628545966, -11584184.510675032, 7827151.696402047, SpatialReferences.getWebMercator()));
    }

    private static TileInfo CreateTileInfo()
    {
        ArrayList<LevelOfDetail> levels = new ArrayList<>();
        double resolution = 11897270.578531113 * 2 / 256;
        double scale = resolution * 96 * 39.37;
        for (int i = 0; i < 19; i++)
        {
            LevelOfDetail l = new LevelOfDetail(i, resolution, scale);
            levels.add(l);
            resolution /= 2;
            scale /= 2;
        }
        return new TileInfo(96, TileInfo.ImageFormat.PNG, levels, new Point(56.130366,-106.346771, SpatialReferences.getWebMercator()), SpatialReferences.getWebMercator(), 256, 256);
    }

    private Task<Uri> GetTileUriAsync(int level, int row, int column, CancellationToken cancellationToken)
    {
        return Task.forResult(Uri.parse(url));
    }
    @Override
    protected String getTileUrl(TileKey tileKey) {
        return url;
    }

    @Override
    public String getUri() {
        return url;
    }
}
