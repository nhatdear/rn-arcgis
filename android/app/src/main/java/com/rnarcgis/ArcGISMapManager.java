package com.rnarcgis;

import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.Log;

import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.geometry.PointCollection;
import com.esri.arcgisruntime.geometry.PolygonBuilder;
import com.esri.arcgisruntime.geometry.Polyline;
import com.esri.arcgisruntime.geometry.PolylineBuilder;
import com.esri.arcgisruntime.geometry.SpatialReference;
import com.esri.arcgisruntime.geometry.SpatialReferences;
import com.esri.arcgisruntime.layers.ArcGISTiledLayer;
import com.esri.arcgisruntime.loadable.LoadStatusChangedEvent;
import com.esri.arcgisruntime.loadable.LoadStatusChangedListener;
import com.esri.arcgisruntime.mapping.ArcGISMap;
import com.esri.arcgisruntime.mapping.Basemap;
import com.esri.arcgisruntime.mapping.Viewpoint;
import com.esri.arcgisruntime.mapping.view.DrawStatusChangedEvent;
import com.esri.arcgisruntime.mapping.view.DrawStatusChangedListener;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.MapView;

import com.esri.arcgisruntime.symbology.SimpleFillSymbol;
import com.esri.arcgisruntime.symbology.SimpleLineSymbol;
import com.esri.arcgisruntime.symbology.SimpleRenderer;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.uimanager.events.RCTEventEmitter;

import java.util.Map;

/**
 * Created by nhatd on 12/6/2017.
 */

public class ArcGISMapManager extends SimpleViewManager<MapView> {
    public static final String REACT_CLASS = "ArcGISMap";

    private static final String DEFAULT_LAYER = "http://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer";

    private static final String DEFAULT_URL = "https://www.arcgis.com/home/webmap/viewer.html?webmap=69fdcd8e40734712aaec34194d4b988c";
    private MapView mapView;
    private ArcGISMapModule arcGISMapModule;
    private ThemedReactContext context;
    //PropertyChangeSupport extentChangeListener = new PropertyChangeSupport(this);

    public String getName() {
        return REACT_CLASS;
    }


    public ArcGISMapManager(ArcGISMapModule arcGISMapModule) {
        this.arcGISMapModule = arcGISMapModule;
    }

    @Override
    protected MapView createViewInstance(ThemedReactContext reactContext) {
        Log.v(REACT_CLASS, "createViewInstance");

        mapView = new MapView(reactContext);
        arcGISMapModule.setMapView(mapView);
        context = reactContext;
        mapView.addDrawStatusChangedListener(new DrawStatusChangedListener() {
            @Override
            public void drawStatusChanged(DrawStatusChangedEvent drawStatusChangedEvent) {
                onReceiveNativeEvent(context,drawStatusChangedEvent.getDrawStatus().toString());
            }
        });
        return mapView;
    }

    @ReactProp(name = "layers")
    public void setLayers(MapView view, @Nullable ReadableArray layers) {
        Log.v(REACT_CLASS, "set layers");
    }

    public void onReceiveNativeEvent(ReactContext reactContext , String text) {
        WritableMap event = Arguments.createMap();
        event.putString("message", text);
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit("onChange", event);
    }

    @ReactProp(name = "mapSource")
    public void setMapSource(MapView view, @Nullable ReadableMap mapSource) {
        ArcGISMap map = new ArcGISMap();
        if (mapSource != null) {

            Double lat = Double.parseDouble(mapSource.getString("lat"));
            Double lon = Double.parseDouble(mapSource.getString("lon"));
            Double zoom = Double.parseDouble(mapSource.getString("zoom"));
            ArcGISTiledLayer tiledLayer = new ArcGISTiledLayer(mapSource.getString("baseurl").replace("/tile/{l}/{y}/{x}/",""));
            Basemap basemap = new Basemap(tiledLayer);
            map.setBasemap(basemap);
            map.setInitialViewpoint(new Viewpoint(lat,lon,zoom));
            mapView.setMap(map);
        } else {
            mapView.setMap(new ArcGISMap(DEFAULT_URL));
        }
    }

    @ReactProp(name = "mapData")
    public void setMapData(MapView view, @Nullable final ReadableMap mapData) {
        if (mapData != null) {
            Double startLat = mapData.getArray("features").getMap(0).getMap("geometry").getArray("coordinates").getArray(0).getArray(0).getDouble(1);
            Double startLon = mapData.getArray("features").getMap(0).getMap("geometry").getArray("coordinates").getArray(0).getArray(0).getDouble(0);
            onReceiveNativeEvent(context,"Map load start");
            final ArcGISMap mMap = new ArcGISMap(Basemap.Type.TOPOGRAPHIC, startLat,startLon, 8);
            mMap.addDoneLoadingListener(new Runnable() {
                @Override
                public void run() {
                    onReceiveNativeEvent(context,"Map load done");
                }
            });
            mMap.addLoadStatusChangedListener(new LoadStatusChangedListener() {
                @Override
                public void loadStatusChanged(LoadStatusChangedEvent loadStatusChangedEvent) {
                    onReceiveNativeEvent(context,loadStatusChangedEvent.getNewLoadStatus().toString());
                }
            });
            mapView.getGraphicsOverlays().clear();
            ReadableArray features = mapData.getArray("features");
            for (int i = 0; i < features.size(); i++){
                ReadableMap feature = features.getMap(i);
                String type = feature.getMap("geometry").getString("type");
                if (type.contains("polyline")) {
                    PolylineBuilder lineGeometry = new PolylineBuilder(SpatialReferences.getWgs84());

                    ReadableArray coors = feature.getMap("geometry").getArray("coordinates").getArray(0);
                    for (int j = 0; j < coors.size(); j++) {

                        ReadableArray data = coors.getArray(j);
                        lineGeometry.addPoint(data.getDouble(0), data.getDouble(1));
                    }
                    ReadableMap properties = feature.getMap("properties");
                    SimpleLineSymbol lineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID,
                            Color.argb(
                                    100,
                                    properties.getMap("color").getInt("r"),
                                    properties.getMap("color").getInt("g"),
                                    properties.getMap("color").getInt("b")), properties.getInt("width"));
                    Graphic lineGraphic = new Graphic(lineGeometry.toGeometry());
                    GraphicsOverlay lineGraphicOverlay = new GraphicsOverlay();
                    SimpleRenderer lineRenderer = new SimpleRenderer(lineSymbol);
                    lineGraphicOverlay.setRenderer(lineRenderer);
                    lineGraphicOverlay.getGraphics().add(lineGraphic);
                    mapView.getGraphicsOverlays().add(lineGraphicOverlay);
                } else if (type.contains("polygon")) {
                    PolygonBuilder builder = new PolygonBuilder(SpatialReferences.getWgs84());

                    ReadableArray coors = feature.getMap("geometry").getArray("coordinates").getArray(0);
                    for (int j = 0; j < coors.size(); j++) {

                        ReadableArray data = coors.getArray(j);
                        builder.addPoint(data.getDouble(0), data.getDouble(1));
                    }
                    ReadableMap properties = feature.getMap("properties");
                    SimpleLineSymbol lineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID,
                            Color.argb(
                                    100,
                                    properties.getMap("color").getInt("r"),
                                    properties.getMap("color").getInt("g"),
                                    properties.getMap("color").getInt("b")), properties.getInt("width"));
                    SimpleFillSymbol fillSymbol =
                            new SimpleFillSymbol(SimpleFillSymbol.Style.DIAGONAL_CROSS,
                                    Color.argb(
                                            255,
                                            properties.getMap("color").getInt("r"),
                                            properties.getMap("color").getInt("g"),
                                            properties.getMap("color").getInt("b")),
                                    lineSymbol);
                    Graphic polygonGraphic = new Graphic(builder.toGeometry());
                    GraphicsOverlay polygonGraphicOverlay = new GraphicsOverlay();
                    // create simple renderer
                    SimpleRenderer polygonRenderer = new SimpleRenderer(fillSymbol);
                    // add graphic to overlay
                    polygonGraphicOverlay.setRenderer(polygonRenderer);
                    // add graphic to overlay
                    polygonGraphicOverlay.getGraphics().add(polygonGraphic);
                    mapView.getGraphicsOverlays().add(polygonGraphicOverlay);
                }
            }
            mapView.setMap(mMap);

        }
    }
}
